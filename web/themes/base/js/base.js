/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
                var slides = new Swiper ('.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'coverflow',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };
    Drupal.behaviors.imagesLoaded = {
        attach: function (context, settings) {
            $('.grid').imagesLoaded(function () {
                $('.grid').masonry({
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    percentPosition: true,
                    gutter: 10,
                    fitWidth: true
                 
                });
            });
        }
    };
    Drupal.behaviors.referencesLightgallery = {
        attach: function (context, settings) {
            $(document).ready(function () {
                $(".grid").lightGallery();
            });
        }
    };

})(jQuery, Drupal);
